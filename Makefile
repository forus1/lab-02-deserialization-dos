include Makefile.d/defaults.mk

all: ## (Default) Clean and test
all: test dist

### BUILD

dist:
	mkdir --parents $@
	echo "<p>All is well</p>" > $@/index.html

%.class: %.java
	javac $<

### TEST

test: ## Test if program is working correctly
test: test-mercury
test: test-venus
test: test-earth
test: test-pluto
test: test-black-hole
	########################################
	#                                      #
	# EVERYTHING IS GOOD! CONGRATULATIONS! #
	#                                      #
	########################################
.PHONY: test

test-mercury: Serialize.class Upload.class
	#
	# ASSERTIONS FOR MERCURY:
	#
	# Serialization and upload was successful:
	#
	java Serialize Mercury \
	| java Upload \
	| timeout 10s grep "Swift planet"
.PHONY: test-mercury

test-venus: Serialize.class Upload.class
	#
	# ASSERTIONS FOR VENUS:
	#
	# Serialization and upload was successful:
	#
	java Serialize Venus \
	| java Upload \
	| grep "Morning star"
	#
	# Correct!
	#
.PHONY: test-venus

test-earth: Serialize.class Upload.class
	#
	# ASSERTIONS FOR EARTH:
	#
	# Serialization and upload was successful:
	#
	java Serialize Earth \
	| timeout 10s java Upload \
	| grep "Blue planet"
	#
	# Correct!
	#
.PHONY: test-earth

test-pluto: pluto.response
	#
	# ASSERTIONS FOR PLUTO:
	#
	# Serialization was successful but upload failed:
	#
	grep "Unsupported class; String" pluto.response
	#
	# Correct!
	#
.PHONY: test-pluto

test-black-hole: black-hole.response
	# ASSERTIONS FOR BLACK HOLE:
	#
	# Serialization and upload was successful:
	#
	grep "Unsupported class; java.util.HashSet" black-hole.response
	#
	# Correct!
	#
.PHONY: test-black-hole

pluto.response: Serialize.class Upload.class

# Pluto is a serialized string. Upload should fail the same way as Black Hole,
# but there is no risk of DoS.

	function handle_exit {
		case $$? in
			0) 		echo "Serialization successful? No way. The Upload program shoud exit with code 1.";;
			1)		exit 0;;
			*) 		echo "Incorrect exit status. The Upload program should exit with code 1.";;
		esac
	}

	trap handle_exit EXIT

	java Serialize "Pluto" \
	| timeout 5s java Upload \
	&> pluto.response

black-hole.response: Serialize.class Upload.class

# Black Hole is a trap. Attempt to deserialize it without extra precautions will
# result in 100% CPU consumption until the process is killed.  So we wrap the
# call to deserialize with a timeout. If it's exceeded the exit status will be
# 124. That's not good. Essentially it means that the program is susceptible to
# DoS attack. What we want is exit status 1.

	function handle_exit {
		case $$? in
			0)
				echo "Serialization successful? No way. The Upload program shoud exit with code 1."
				exit 1
				;;

			1)
				exit 0;;
			124)
				echo "DoS attack succesfull. A deserialization black hole swallowed you.";;
			*)
				echo "Incorrect exit status. The Upload program should exit with code 1.";;
		esac
	}

	trap handle_exit EXIT

# We are capturing both standard output and standard error for later inspection.

	java Serialize "Black Hole" \
	| timeout 5s java Upload \
	&> black-hole.response

### INSTALL

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install

### DEVELOPMENT

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX
.PHONY: clean

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)
